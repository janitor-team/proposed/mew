#!/usr/bin/make -f

#export DH_VERBOSE=1

export DH_OPTIONS

PACKAGE=`cat debian/PACKAGE`
PKGSNAME=mew
CHG_SUFFIX=`test -f 00changes || ls -1 00changes.* | sed s/00changes// | sort -n -t . -k 2,2 -k 3 | tail -1`

include /usr/share/dpkg/architecture.mk

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
dpkg_buildflags = DEB_BUILD_MAINT_OPTIONS=$(DEB_BUILD_MAINT_OPTIONS) dpkg-buildflags
CFLAGS=$(shell $(dpkg_buildflags) --get CFLAGS)
CPPFLAGS=$(shell $(dpkg_buildflags) --get CPPFLAGS)
LDFLAGS=$(shell $(dpkg_buildflags) --get LDFLAGS)

configure-stamp: configure
	dh_testdir
	CFLAGS="$(CFLAGS)" CPPFLAGS="$(CPPFLAGS)" LDFLAGS="$(LDFLAGS)" ./configure --prefix=/usr --infodir=/usr/share/info --mandir=/usr/share/man --build=$(DEB_BUILD_GNU_TYPE) --host=$(DEB_HOST_GNU_TYPE)
	grep '^Package: ' debian/control | head -1 | sed -e "s/^Package: //g" > debian/PACKAGE
	sed -e "s/@PACKAGE@/$(PACKAGE)/g" debian/README.Debian.in > debian/README.Debian
	sed -e "s/@PACKAGE@/$(PACKAGE)/g" debian/dirs.in > debian/dirs
	sed -e "s/@PACKAGE@/$(PACKAGE)/g" debian/emacsen-install.in > debian/emacsen-install
	sed -e "s/@PACKAGE@/$(PACKAGE)/g" debian/emacsen-remove.in > debian/emacsen-remove
	sed -e "s/@PACKAGE@/$(PACKAGE)/g" debian/emacsen-startup.in > debian/emacsen-startup
	ls -1 00* | egrep -v '(^00copyright$$|^00changes)' > debian/docs
	ls -1 debian/dot.* dot.* > debian/examples
	ls -1 info/mew.*info* > debian/info
	touch $@

build: build-arch build-indep

build-arch: build-arch-stamp
build-arch-stamp: configure-stamp
	$(MAKE) bin
	touch $@

build-indep: build-indep-stamp
build-indep-stamp: configure-stamp
	touch $@

clean:
	dh_testdir
	dh_testroot
	rm -f build-arch-stamp build-indep-stamp configure-stamp debian/PACKAGE debian/README.Debian debian/dirs debian/emacsen-install debian/emacsen-remove debian/emacsen-startup debian/docs debian/examples debian/info
	[ ! -f Makefile ] || $(MAKE) distclean
	dh_clean

install: install-arch install-indep
install-indep: DH_OPTIONS=-i
install-indep:
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs
	cp mew*.el debian/$(PACKAGE)/usr/share/$(PKGSNAME)
	for f in debian/mew*.el; do \
	  if [ -f "$$f" ]; then \
	    cp "$$f" debian/$(PACKAGE)/usr/share/$(PKGSNAME); \
	  fi; \
	done
	for f in 00changes.*; do \
	  if [ -f "$$f" ]; then \
	    cp "$$f" debian/$(PACKAGE)/usr/share/doc/$(PACKAGE)/changelog.`echo $$f | sed -e s/00changes.//`; \
	  fi; \
	done
	rm -f debian/$(PACKAGE)/usr/share/doc/$(PACKAGE)/changelog$(CHG_SUFFIX)
	cp -r etc/* debian/$(PACKAGE)/usr/share/pixmaps/$(PKGSNAME)
	if [ -d contrib ]; then \
	  mkdir debian/$(PACKAGE)/usr/share/$(PKGSNAME)/contrib; \
	  cp contrib/mew*.el debian/$(PACKAGE)/usr/share/$(PKGSNAME)/contrib; \
	  rm -f debian/$(PACKAGE)/usr/share/$(PKGSNAME)/contrib/mew-toolbar-frame.el; \
	fi
	for f in debian/$(PACKAGE)/usr/share/$(PKGSNAME)/*.el \
	         debian/$(PACKAGE)/usr/share/$(PKGSNAME)/contrib/*.el; do \
	  if [ -f "$$f" ]; then \
	    perl -pi -e 'if ($$. == 1 && !/no-native-compile:/) { if (!/;/) { s/^/;;; -*--*-\n/ } elsif (!/-\*-.*-\*-/) { s/$$/ -*--*-/ } s/-\*- *emacs-lisp *-\*-/-*-mode: emacs-lisp;-*-/i; s/-\*-/-*-no-native-compile: t;/ }' "$$f"; \
	  fi; \
	done
	for f in alpha beta; do \
	  if [ -d "$$f" ]; then \
	    cp -r "$$f" debian/$(PACKAGE)/usr/share/doc/$(PACKAGE); \
	  fi; \
	done

install-arch: DH_OPTIONS=-a
install-arch:
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs
	$(MAKE) DESTDIR=$(CURDIR)/debian/$(PACKAGE)-bin install-bin
	install -m 755 debian/mewstunnel debian/$(PACKAGE)-bin/usr/bin
	install -m 644 debian/mewstunnel.1 debian/$(PACKAGE)-bin/usr/share/man/man1
	rm -f debian/$(PACKAGE)-bin/usr/bin/mewdecode
	rm -f debian/$(PACKAGE)-bin/usr/bin/mewcat
	rm -f debian/$(PACKAGE)-bin/usr/share/man/man1/mewdecode.1*
	rm -f debian/$(PACKAGE)-bin/usr/share/man/man1/mewcat.1*

binary-indep: DH_OPTIONS=-i
binary-indep: build-indep install-indep
	dh_testdir
	dh_testroot
#	dh_installdebconf
	dh_installdocs
	dh_installexamples
	dh_installmenu
	dh_installemacsen
	dh_installman
	dh_installinfo
	dh_installchangelogs 00changes$(CHG_SUFFIX)
	dh_link -p$(PACKAGE) usr/share/pixmaps/$(PKGSNAME) usr/share/$(PKGSNAME)/etc
	dh_compress
	dh_fixperms
	dh_installdeb
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary-arch: DH_OPTIONS=-a
binary-arch: build-arch install-arch
	dh_testdir
	dh_testroot
#	dh_installdebconf
	dh_installdocs
	dh_installexamples
	dh_installmenu
	dh_installman
	dh_installinfo
	dh_installchangelogs 00changes$(CHG_SUFFIX)
	dh_strip
	dh_link -p$(PACKAGE)-bin usr/bin/mewencode usr/bin/mewdecode
	dh_link -p$(PACKAGE)-bin usr/bin/mewencode usr/bin/mewcat
	dh_link -p$(PACKAGE)-bin usr/share/man/man1/mewencode.1 usr/share/man/man1/mewdecode.1
	dh_link -p$(PACKAGE)-bin usr/share/man/man1/mewencode.1 usr/share/man/man1/mewcat.1
	dh_compress
	dh_fixperms
	chgrp mail debian/$(PACKAGE)-bin/usr/bin/incm
	chmod g+s  debian/$(PACKAGE)-bin/usr/bin/incm
	dh_lintian
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install install-indep install-arch build-indep build-arch
